// Address of Block Chain: https://api.blockcypher.com/v1/eth/main/addrs/ADDRESS/balance
//  curl -s https://api.blockcypher.com/v1/eth/main/addrs/738d145faabb1e00cf5a017588a9c0f998318012/balance
const rp = require('request-promise');
const Promise = require('bluebird');
const Big = require('big.js');

/**
 * Converts Weis to Ether currency value
 * @param {Number} weis 
 * @returns {Number} ether
 */
var convertWeisToEther = (weis) => {
  let weisCurrency = Big(weis);
  let divisor = Big(10).pow(18);
  let ether = weisCurrency.div(divisor);
  return ether;
};

/**
 * GETS JSON Data from an Ethereum Account and resolves the promise
 * @param {String} address 
 * @returns {Promise} jsondata
 */
var getEthereumDetails = (uri) => {
  let options = {
    uri: uri,
    json: true
  };
  return new Promise((resolve, reject) => {
    rp(options).then((ethereumDetails) => {
      resolve(ethereumDetails);
    }).catch((error) => {
      // reject(error);
      resolve(error);
    });
  });
}

/**
 * POST /ethereum
 * Displays the balance of a requested Ethereum Account
 * @param {*} req 
 * @param {*} res 
 */
exports.addressDetails = (req, res) => {
  let address = req.body.address;
  let uri = `https://api.blockcypher.com/v1/eth/main/addrs/${address}/balance`;
  // Get the json body
  Promise.all([getEthereumDetails(uri)]).spread((ethereumDetails) => {
    if(ethereumDetails.name) {
      // return res.json(ethereumDetails);
      let balance = null;
      return ethereumView(res, ethereumDetails, balance);
    }
    else {
      let balance = convertWeisToEther(ethereumDetails.balance);
      return ethereumView(res, ethereumDetails, balance);
    }
  });
};
/**
 * GET /ethereum
 * Renders the Initial Ethereum Form
 * @param {*} req 
 * @param {*} res 
 */
exports.getDetails = (req, res) => {
  let ethereumDetails = null;
  let balance = null;
  ethereumView(res, ethereumDetails, balance);
};

/**
 * Renders the Ethereum Form
 * @param {*} res 
 * @param {JSON} ethereumDetails
 * @param {Number} balance
 */
var ethereumView = (res, ethereumDetails, balance) => {
  res.render('home/ethereum', {
    title: 'Ethereum',
    etheriumDetails: ethereumDetails,
    balance: balance
  });
}
exports.convertWeisToEther = convertWeisToEther;
exports.getEthereumDetails = getEthereumDetails;
exports.ethereumView = ethereumView;