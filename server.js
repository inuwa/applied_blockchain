const express = require('express');
const pug = require('pug');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
const app = express();

const index = require('./app/controllers/index');
const ethereum = require('./app/controllers/ethereum');

// Use Pug
app.set('views', './views');
app.set('view engine', 'pug');

// Static Files
app.use(express.static('./public'));

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride((req, res) => {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    var method = req.body._method;
    delete req.body._method;
    return method;
  }
}));

// Routes
app.get('/ethereum', ethereum.getDetails);
app.get('/index', index.index);
app.post('/ethereum', ethereum.addressDetails);


/**
 * Error Handling
 */
app.use((err, req, res, next) => {
  // treat as 404
  if (err.message
    && (~err.message.indexOf('not found')
    || (~err.message.indexOf('Cast to ObjectId failed')))) {
    return next();
  }
  console.error(err.stack);
  // error page
  res.status(500).render('500', { error: err.stack });
});

// assume 404 since no middleware responded
app.use((req, res, next) => {
  res.status(404).render('404', {
    url: req.originalUrl,
    error: 'Not found'
  });
});

const server = app.listen(process.env.PORT || 3000, () => {
  const port = server.address().port;
    console.log("Server is listening on port " + port);
});

module.exports = server;
