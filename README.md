[![Build Status](https://img.shields.io/travis/madhums/node-express-mongoose.svg?style=flat)](https://travis-ci.org/madhums/node-express-mongoose)
[![Dependencies](https://img.shields.io/david/madhums/node-express-mongoose.svg?style=flat)](https://david-dm.org/madhums/node-express-mongoose)
[![Code Climate](https://codeclimate.com/github/madhums/node-express-mongoose/badges/gpa.svg)](https://codeclimate.com/github/madhums/node-express-mongoose)
[![Greenkeeper badge](https://badges.greenkeeper.io/madhums/node-express-mongoose.svg)](https://greenkeeper.io/)
[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/madhums/node-express-mongoose?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![Gittip](https://img.shields.io/gratipay/madhums.svg?style=flat)](https://www.gratipay.com/madhums/)

## Ethereum Balance fetcher from third-party API 

#### **Assignment:** Develop a simple NodeJS Express app with a Jade (Pug) view/page to get the balance of an Ethereum address. It will need to perform a single external request (to an API that provides such data) using a promise-enabled library and async/await.

The view/page needs to have a form that when submitted will add an "address" GET request parameter to the url. The form will have only one text input field (labeled "Address"), representing the Ethereum address, no styling/CSS work needed.

The express route that responds to the form request (can be the same route or you can make two) needs to query an external API service (Blockcypher.com) using the current JSON API URL: `https://api.blockcypher.com/v1/eth/main/addrs/ADDRESS/balance`.

Example of the request (using curl):

    curl -s https://api.blockcypher.com/v1/eth/main/addrs/738d145faabb1e00cf5a017588a9c0f998318012/balance

Blockchypher endpoint documentation:

    https://dev.blockcypher.com/eth/ > Address API > Address Balance Endpoint

The route will need to make the request using a promise-based library like Axios or a node `fetch()`-based (like `node-fetch`) library and Async/Await.

The page rendered from the GET response will display the balance of the address, a numeric value denominated in ETH, Ethers. In the case of the `'738d145faabb1e00cf5a017588a9c0f998318012'` address the 'balance' value coming from the API is `19254563000000000` and it's the one to use. 

That balance will need to be displayed in the **crypto-currency denomination Ether** instead of Weis (`1 Ether == 10^18 weis`) meaning that for that address it should show a balance of `0.019254563` ETHs.

## What has been done
I have created an app that displays a form at  `/ethereum` once you enter an address submit it. It returns a name of Ether Balance. 

## Usage
  **Start Application**

    $ git clone https://bitbucket.org/inuwa/applied_blockchain
    $ cd applied_blockchain
    $ yarn install
    $ yarn start

  **Run Tests**

    $ yarn test

