const assert = require('assert');
var chai = require('chai')
  , expect = chai.expect
  , should = chai.should();
const ethereum = require('../app/controllers/ethereum');
const Promise = require('bluebird');
const rp = require('request-promise');

describe('Ethereum Tests', () => {
	// And then we describe our testcases.
	it('Should Convert  19254563000000000 Weis to 0.019254563 Ether ', (done) => {
    let ether = ethereum.convertWeisToEther(19254563000000000);
    assert.equal(ether, 0.019254563, 'Ether value = 0.01925456');
		// Invoke done when the test is complete.
		done();
  });
  it('Should Return An Error Response Wallet ADDRESS not found', () => {
    let uri = `https://api.blockcypher.com/v1/eth/main/addrs/ADDRESS/balance`;
    let getEthereumDetails =  ethereum.getEthereumDetails(uri);
    Promise.all([getEthereumDetails]).spread((ethereumDetails) => {
      expect(ethereumDetails.name).to.equal('StatusCodeError');
      expect(ethereumDetails.response.body.error).to.equal('Wallet ADDRESS not found');
    });
  });
  it('Should return Ethereum  Balance of 19254563000000000', () => {
    let uri = `https://api.blockcypher.com/v1/eth/main/addrs/738d145faabb1e00cf5a017588a9c0f998318012/balance`
    let options = {
      uri: uri,
      json: true
    };
    return rp(options).then((ethereumDetails) => {
      expect(ethereumDetails.balance).to.equal(19254563000000000);
    })
    .catch();
  });
  it('Ethereum Balance should return a number', () => {
    let uri = `https://api.blockcypher.com/v1/eth/main/addrs/738d145faabb1e00cf5a017588a9c0f998318012/balance`
    let options = {
      uri: uri,
      json: true
    };
    return rp(options).then((ethereumDetails) => {
      expect(ethereumDetails.balance).to.be.a('Number');
    })
    .catch();
  });
});
